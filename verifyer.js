import jwt from 'jwt'
import crypto from 'node:crypto';
import secretKey from './config.js'
import { selectAccount } from '../integration/accounts.js';

function hash(string, salt) {
    const hashedPassword = crypto
        .createHash('sha512')
        .update(string + salt)
        .digest('hex')
    return `sha512$${salt}$${hashedPassword}`
}

export async function verifyUser(username, password) {
    const account = await selectAccount(username)
    if (!account) {
        return false // Invalid login
    }

    const [_algo, salt, _passhash] = account.passhash.split('$')
    return account.passhash === hash(password, salt)
}

export async function assertToken(req, res) {
    try {
        if (!req.token) {
            res.status(401).send('Unauthorized (no token passed)')
            return
        }
        jwt.verify(req.token, secretKey);
    } catch (err) {
        res.status(401).send(err.message)
    }
}