import {db_string} from './config.js'
import knex from 'knex'

export const employee_table = 'employes'
export const user_table = 'users'

export const Knex = knex({
    client: 'pg',
    connection: db_string
})
