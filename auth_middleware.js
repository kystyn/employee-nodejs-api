import jwt from 'jsonwebtoken'
import { secretKey } from './config.js'

export function verifyToken(req, res, next) {
    let token = req.headers['authorization']

    if (token) {
        token = token.split(' ')[1]
        jwt.verify(token, secretKey, (err, decoded) => {
            if (err) {
                res.status(401).json({message: "Failed to authenticate user jwt token"})
            } else {
                next()
            }
        })
    }
    else {
        res.status(401).json({message: "No user jwt token provided"})
    }
}