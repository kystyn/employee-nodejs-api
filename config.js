import crypto from 'node:crypto'

export const port = 3000
export const address = '/employes'
export const db_string = process.env.EMPLOYEE_DB_STRING
export const pageSize = 5
export const tokenTTL = 300 // seconds
export const secretKey = process.env.JWT_SECRET_KEY

export const Status = { OK: 0, NOT_FOUND: 1, ALREADY_EXISTS: 2 }

export function getStatus(status) {
    switch (status)
    {
    case Status.OK:
        return 200
    case Status.ALREADY_EXISTS:
        return 400
    case Status.NOT_FOUND:
        return 404
    }
}