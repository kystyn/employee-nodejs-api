import {Knex, employee_table} from './db.js'
import {Status, pageSize} from './config.js'

async function selectAll(key, sortSalary, page) {
    console.log('selectAll called');
    let query = Knex.select().from(employee_table);

    if (sortSalary != undefined) {
        if (sortSalary === 'ASC' || sortSalary === 'DESC')
            query = query.orderBy('salary', sortSalary);
    }

    if (key != undefined) {
        // search for any enter. by name, surname, both
        // array when equest to collection (filter by ...)
        query.where(Knex.raw('name ILIKE ?', `%${key}%`))
        .orWhere(Knex.raw('surname ILIKE ?', `%${key}%`))
    }

    if (page != undefined) {
        const cnt = await Knex.select().from(employee_table).count()
        const pageCount = Math.ceil(Number(cnt[0].count) / pageSize)
        if (page < 1 || page > pageCount) {
            return {
                status: Status.OK,
                message: []
            }
        }

        query.offset((page - 1) * pageSize).limit(pageSize)
    }

    return {
        status: Status.OK,
        message: await query
    }
}

async function selectById(id) {
    const query = await Knex.select().from(employee_table).
        where({'id': id})

    return query.length > 0 ? 
        {
            status: Status.OK,
            message: query[0]
        } :
        {
            status: Status.NOT_FOUND,
            message: "Element with " + id.toString() + " id not found"
        }
}

async function insert(queryText) {
    await Knex.insert(queryText).into(employee_table)
    return {status: Status.OK, message: "Successful insert"}
}

async function updateById(id, queryText) {
    await Knex(employee_table).where({'id': id}).update(queryText)
    return {status: Status.OK, message: "Successful update"}
}

async function deleteById(id) {
    const query = await Knex.delete().from(employee_table).
            where({'id': id})
    return query == 1 ? {
        status: Status.OK,
        message: "Successful delete"
    } : {
        status: Status.NOT_FOUND,
        message: "No element to delete"
    }
}

export {selectAll, selectById, insert, deleteById, updateById}
