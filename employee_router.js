import {address, getStatus} from './config.js'
import {getterSchema, employeeSchema} from './validator.js'
import express from 'express'
import {selectAll, selectById, insert, deleteById, updateById} from './employee_controller.js'
import {verifyToken} from './auth_middleware.js'
import asyncHandler from 'express-async-handler'

// ATTENTION!!!
// к entity -- 404, к коллекции -- нельзя 404

let employeeRouter = express.Router()

// NO 404 PLS
employeeRouter.route(address)
    /* Select * */
    .get(asyncHandler(async (req, res) => {
        let {error, value} = getterSchema.validate(req.query)

        if (error) {
            res.status(400).json({message: error.message})
        }
        else {
            let queryRes = await selectAll(req.query.key, req.query.sortSalary, req.query.page)

            res.status(getStatus(queryRes.status))
                .json(queryRes.message)
        }
    }))
    /* Insert */
    .post(verifyToken, asyncHandler(async (req, res) => {
        let {error, value} = employeeSchema.validate(req.body)

        if (error) {
            res.status(400).json({message: error.message})
        }
        else {
            let queryRes = await insert(req.body)
            // 200
            res.status(getStatus(queryRes.status))
                .json({message: queryRes.message})
        }
    }))

// OH YES 404 WELCOME
employeeRouter.route(address + '/:id')
    /* Select by id */
    .get(asyncHandler(async (req, res) => {
        let queryRes = await selectById(req.params.id)

        // 200 | 404
        res.status(getStatus(queryRes.status))
            .json({message: queryRes.message})
    }))
    /* Delete by id */
    .delete(verifyToken, asyncHandler(async (req, res) => {
        let queryRes = await deleteById(req.params.id)

        // 200 | 404
        res.status(getStatus(queryRes.status))
            .json({message: queryRes.message})
    }))
    .put(verifyToken, asyncHandler(async (req, res) => {
        let {error, value} = employeeSchema.validate(req.body)

        if (error) {
            res.status(400).json({message: error.message})
        }
        else {
            let queryRes = await updateById(req.params.id, req.body)
            // 200
            res.status(getStatus(queryRes.status))
                .json({message: queryRes.message})
        }
    }))


export default employeeRouter
