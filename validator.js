import joi from 'joi'

export const employeeSchema = joi.object({
    name: joi.string()
        .alphanum()
        .max(40)
        .required(),
    
    surname: joi.string()
        .alphanum()
        .max(40)
        .required(),

    position: joi.string()
        .lowercase()
        .valid('junior', 'middle', 'senior', 'techlead', 'teamlead')
        .required(),

    birthday: joi.date()
        .required(),

    salary: joi.number()
        .positive()
        .required()
});

export const getterSchema = joi.object({
    key: joi.string()
        .alphanum()
        .max(40),
    
    sortSalary: joi.string()
        .valid('ASC', 'DESC'),

    page: joi.number()
});

export const userSchema = joi.object({
    username: joi.string()
        .alphanum()
        .max(40)
        .required(),
    
    passwd: joi.string()
        .alphanum()
        .min(4)
        .max(40)
        .required()
});

//export default {userSchema, authSchema}