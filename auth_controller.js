import {Knex, user_table} from './db.js'
import {Status} from './config.js'

async function selectUser(userName) {
    let query = await Knex.select().from(user_table).where({
        'username': userName
    })

    if (query.length == 0)
        return {status: Status.NOT_FOUND, message: "User not found"}

    return {status: Status.OK, message: query}
}

async function createUser(userName, passwd) {
    // TODO check how res behaves when create user
    await Knex.insert({
        'username': userName,
        'passwd': passwd
    }).into(user_table)

    return {status: Status.OK, message: "Successfully created user"}
}

export {selectUser, createUser}