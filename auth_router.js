import jwt from 'jsonwebtoken'
import express from 'express'
import { userSchema } from './validator.js'
import { createUser, selectUser } from './auth_controller.js'
import { Status, secretKey, tokenTTL, getStatus } from './config.js'
import asyncHandler from 'express-async-handler'
import passwordHash from 'password-hash'

let authRouter = express.Router()

authRouter.post('/auth', asyncHandler(async (req, res) => {
    // TODO catch errors
    // TODO validate /auth?username=...&passwd=...

    let {error, value} = userSchema.validate(req.body)

    if (error)
        res.status(400).send({message: error.message})
    else {
        // TODO hash passwd
        let userQuery = await selectUser(req.body.username)
        if (!passwordHash.verify(req.body.passwd, userQuery.message[0].passwd)) {
            res.status(401).json({message: "Incorrect user/passwd pair"})
            return
        }

        if (userQuery.status == Status.OK) {
            let payload = { username: req.body.username }
            let token = jwt.sign(payload, secretKey, {expiresIn: tokenTTL})
            res.json({token: token})
        }
        else {
            res.status(401).json({message: userQuery.message})
        }
    }
}))

authRouter.post('/register', asyncHandler(async (req, res) => {
    let {error, value} = userSchema.validate(req.body)

    if (error)
        res.status(400).send({message: error.message})
    else {
        const queryRes = await createUser(req.body.username, passwordHash.generate(req.body.passwd))

        // TODO hash passwd
        res.status(getStatus(queryRes.status)).send({message: queryRes.message})
    }
}))

export default authRouter