import {port} from './config.js'
import employeeRouter from './employee_router.js'
import authRouter from './auth_router.js'
import express from 'express'
import cors from 'cors'

const app = express()

app.use(express.json());
app.use(cors());
app.use(employeeRouter);
app.use(authRouter);
app.use((error, req, res, next) => {
	res.status(500).json({error: "Server error: " + error.message})
});

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`);
});

